# requires: Java, Maven, PowerShell, Permission to run PS scripts
# permissions for this PS session only:   Set-ExecutionPolicy -ExecutionPolicy Bypass -Scope Process

# exit if cmdlet gives error
$ErrorActionPreference = "Stop"

# Check to see if root CA file exists, download if not
If (!(Test-Path ".\root-CA.crt")) {
    "`nDownloading AWS IoT Root CA certificate from Symantec..."
    Invoke-WebRequest -Uri https://www.symantec.com/content/en/us/enterprise/verisign/roots/VeriSign-Class%203-Public-Primary-Certification-Authority-G5.pem -OutFile root-CA.crt
}

If (!(Test-Path ".\aws-iot-device-sdk-java")) {
    "`nInstalling AWS SDK..."
    git clone https://github.com/aws/aws-iot-device-sdk-java.git
    cd aws-iot-device-sdk-java
    mvn install --% -Dgpg.skip=true
    cd ..
}

"`nRunning the pub/sub sample application..."
cd aws-iot-device-sdk-java
mvn exec:java -pl aws-iot-device-sdk-java-samples --% -Dexec.mainClass="com.amazonaws.services.iot.client.sample.pubSub.PublishSubscribeSample" -Dexec.args="-clientEndpoint a1qm5y6w34w519.iot.us-west-2.amazonaws.com -clientId sdk-java -certificateFile ..\myWindows.cert.pem -privateKeyFile ..\myWindows.private.key"
