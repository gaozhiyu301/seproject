package com.baeldung.lambda.dynamodb;

import com.amazonaws.services.iot.client.AWSIotException;
import com.amazonaws.services.iot.client.AWSIotMessage;
import com.amazonaws.services.iot.client.AWSIotMqttClient;
import com.amazonaws.services.iot.client.AWSIotQos;
import com.amazonaws.services.iot.client.AWSIotTimeoutException;
import com.amazonaws.services.iot.client.AWSIotTopic;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.baeldung.lambda.dynamodb.bean.CommandArguments;
import com.baeldung.lambda.dynamodb.bean.PersonRequest;
import com.baeldung.lambda.dynamodb.bean.PersonResponse;
import com.baeldung.lambda.dynamodb.bean.SampleUtil;
import com.baeldung.lambda.dynamodb.bean.SampleUtil.KeyStorePasswordPair;
import com.baeldung.lambda.dynamodb.bean.TestTopicListener;

public class ReplyToIOT  implements RequestHandler<PersonRequest, PersonResponse> {

    private static final String TestTopic = "sdk/test/java2";
    private static final AWSIotQos TestTopicQos = AWSIotQos.QOS0;
    private static AWSIotMqttClient awsIotClient;
    //Later maybe we need to put in the json
    String[] args={"-clientEndpoint", "a1qm5y6w34w519.iot.us-west-2.amazonaws.com", "-clientId", "sdk-java", "-certificateFile", "myWindows.cert.pem", "-privateKeyFile", "myWindows.private.key"};



    public PersonResponse handleRequest(PersonRequest personRequest, Context context) {
        
        CommandArguments arguments = CommandArguments.parse(args);
        initClient(arguments);

        try {
			awsIotClient.connect();
		} catch (AWSIotException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        AWSIotTopic topic = new TestTopicListener(TestTopic, TestTopicQos);
        try {
			awsIotClient.subscribe(topic, true);
		} catch (AWSIotException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        run(personRequest);

        PersonResponse personResponse = new PersonResponse();
        personResponse.setMessage("Saved Successfully!!!");
        return personResponse;
    }
    
    
    public void run(PersonRequest personRequest) {
    	int counter = 1; 
        String firstName = personRequest.getFirstName();
        String lastName = personRequest.getLastName();
        int age =  personRequest.getAge();
        String address =  personRequest.getAddress();

            String payload = "{  \"id\": "+counter++ +",  \"firstName\": \"2\",  \"lastName\": \"3\",  \"age\": 30,  \"address\": \""+firstName + lastName + age +address +"\"}";
            try {
                awsIotClient.publish(TestTopic, payload);
            } catch (AWSIotException e) {
                System.out.println(System.currentTimeMillis() + ": publish failed for " + payload);
            }
            System.out.println(System.currentTimeMillis() + ": >>> " + payload);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println(System.currentTimeMillis() + ": BlockingPublisher was interrupted");
                return;
            }
        }
    

    private void initClient(CommandArguments arguments) {
        String clientEndpoint = arguments.getNotNull("clientEndpoint", SampleUtil.getConfig("clientEndpoint"));
        String clientId = arguments.getNotNull("clientId", SampleUtil.getConfig("clientId"));

        String certificateFile = arguments.get("certificateFile", SampleUtil.getConfig("certificateFile"));
        String privateKeyFile = arguments.get("privateKeyFile", SampleUtil.getConfig("privateKeyFile"));
        if (awsIotClient == null && certificateFile != null && privateKeyFile != null) {
            String algorithm = arguments.get("keyAlgorithm", SampleUtil.getConfig("keyAlgorithm"));

            KeyStorePasswordPair pair = SampleUtil.getKeyStorePasswordPair(certificateFile, privateKeyFile, algorithm);

            awsIotClient = new AWSIotMqttClient(clientEndpoint, clientId, pair.keyStore, pair.keyPassword);
        }

        if (awsIotClient == null) {
            String awsAccessKeyId = arguments.get("awsAccessKeyId", SampleUtil.getConfig("awsAccessKeyId"));
            String awsSecretAccessKey = arguments.get("awsSecretAccessKey", SampleUtil.getConfig("awsSecretAccessKey"));
            String sessionToken = arguments.get("sessionToken", SampleUtil.getConfig("sessionToken"));

            if (awsAccessKeyId != null && awsSecretAccessKey != null) {
                awsIotClient = new AWSIotMqttClient(clientEndpoint, clientId, awsAccessKeyId, awsSecretAccessKey,
                        sessionToken);
            }
        }

        if (awsIotClient == null) {
            throw new IllegalArgumentException("Failed to construct client due to missing certificate or credentials.");
        }
    }

}